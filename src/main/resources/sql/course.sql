
-- ----------------------------
DROP TABLE IF EXISTS `email`;
CREATE TABLE `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of email
-- ----------------------------
INSERT INTO `email` VALUES ('1', 'qushencn@qq.com');

-- ----------------------------


DROP TABLE IF EXISTS `socket`;
CREATE TABLE `socket` (
  `id` varchar(255) NOT NULL,
  `mc` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `prot` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ifsocket` int(11) DEFAULT NULL,
  `ifdelte` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `socket` VALUES ('5b963005-eb6b-4f5a-8dda-ebaeda447423', '测试服务端口', '127.0.0.1', '3307', '离线', '2020-12-21 14:28:06', '1', '1');
INSERT INTO `socket` VALUES ('779d70e1-c9f3-4a6d-91b6-49b2ec481eaa', '测试数据库', '127.0.0.1', '3306', '在线', '2020-12-21 14:28:06', '1', '1');

