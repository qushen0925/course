package com.sws.course.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
@Data
@Accessors(chain = true)
public class Socket {

    private String id;
    private String mc;
    private String ip;
    private String prot;
    private String status;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date time;
    private int ifsocket;
    private int ifdelte;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getProt() {
        return prot;
    }

    public void setProt(String prot) {
        this.prot = prot;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getIfsocket() {
        return ifsocket;
    }

    public void setIfsocket(int ifsocket) {
        this.ifsocket = ifsocket;
    }

    public int getIfdelte() {
        return ifdelte;
    }

    public void setIfdelte(int ifdelte) {
        this.ifdelte = ifdelte;
    }
}
