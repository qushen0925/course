package com.sws.course.controller;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.sws.course.dao.SocketDao;
import com.sws.course.dto.Socket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/Socket")
@CrossOrigin
@Slf4j
public class SocketController {
    @Autowired
    private SocketDao dao;

    @PostMapping("add")
    public Map<String,Object> add(String mc,String ip,String prot) {
        UUID uuid= UUID.randomUUID();
        String uuidString=uuid.toString();
        dao.insertSocket(uuidString,mc,ip,prot,"待定",1,1);
        Map<String, Object> map =  new HashMap<>();
        map.put("success","保存成功");
        map.put("ip",ip);
        map.put("prot",prot);
        return map;
    }

    @PostMapping("findAll")
    public List<Socket> findAll() {

        List<Socket> list=dao.findAll();

        return list;
    }

    @PostMapping("deleteById")
    public Map<String,Object> delete(String id) {
        Map<String, Object> map =  new HashMap<>();
        int num=dao.deleteById(id);
        map.put("success","删除成功");
        return map;
    }



}
