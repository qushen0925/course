package com.sws.course.dao;



import com.sws.course.dto.Socket;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.Date;
import java.util.List;

@Mapper
public interface SocketDao {

    //查询所有需要监控的ip端口
    List<Socket> findAll();

    //插入所有需要监控的ip端口
    int insertSocket(@Param("id") String id,
                     @Param("mc") String mc,
                     @Param("ip") String ip,
                     @Param("prot") String prot,
                     @Param("status") String status,
                     @Param("ifsocket") int ifsocket,
                     @Param("ifdelte") int ifdelte);


    //修改服务端口的状态
    int updateSocket(@Param("id") String id,
                     @Param("mc") String mc,
                     @Param("ip") String ip,
                     @Param("prot") String prot,
                     @Param("status") String status,
                     @Param("time") Date time);


   //查询表中是离线的服务
    List<Socket> findbyStatus();

  //查询要发送的邮箱
    List<String> findEmail();

    //根据id删除相关数据
    int deleteById(String id);

}
