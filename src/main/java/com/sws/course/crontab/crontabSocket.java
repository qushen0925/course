package com.sws.course.crontab;


import com.sws.course.dao.SocketDao;
import com.sws.course.dto.Socket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.ConnectException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class crontabSocket {
    @Autowired
    SocketDao dao;

    @Scheduled(cron="0 0/1 * * * ? ")   //每1分钟执行一次
    public  void AnimalAechartsMapper() throws IOException, ParseException {
        java.net.Socket client = null;
        List<Socket> list=dao.findAll();
        for (int j=0;j<list.size();j++){

            String id=list.get(j).getId();
            String ip=list.get(j).getIp();
            String mc=list.get(j).getMc();
            String prot=list.get(j).getProt();
            int protInt=Integer.parseInt(prot);
            try{
                client = new java.net.Socket(ip, protInt);
                System.out.println("连接已建立...");

                Date time=getDate();
                dao.updateSocket(id,mc,ip,prot,"在线",time);

            }  catch(ConnectException e) {
                System.out.println("未建立连接！！！");

                Date time=getDate();
                dao.updateSocket(id,mc,ip,prot,"离线",time);


            }


        }

    }

    public Date getDate() throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        String dateName = df.format(calendar.getTime());
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = sf.parse(dateName);
        return  date;
    }

}
