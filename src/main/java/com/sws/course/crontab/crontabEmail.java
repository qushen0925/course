package com.sws.course.crontab;

import com.sws.course.dao.SocketDao;
import com.sws.course.dto.Socket;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;


@Component
public class crontabEmail {

    @Autowired
    SocketDao socketdao;

    @Scheduled(cron="0/5 * * * * ?")   //每10分钟执行一次
    public  void SendEamil() throws IOException, ParseException, EmailException {

        List<Socket> list=socketdao.findbyStatus();
        List<String>  emailList =socketdao.findEmail();

        int num=list.size();
        if (num>0) {
            HtmlEmail email=new HtmlEmail();//创建一个HtmlEmail实例对象
            email.setHostName("smtp.163.com");//邮箱的SMTP服务器，一般123邮箱的是smtp.123.com,qq邮箱为smtp.qq.com
            email.setCharset("utf-8");//设置发送的字符类型

            for (int i=0;i<emailList.size();i++){
                email.addTo(emailList.get(i));//设置收件人
            }

            email.setFrom("18629461220@163.com","服务端口监测系统");//发送人的邮箱为自己的，用户名可以随便填
            email.setAuthentication("18629461220@163.com","AWGIHRBEBSVPURHA");//设置发送人到的邮箱和用户名和授权码(授权码是自己设置的)
            email.setSubject("服务端口监测系统-离线通知");//设置发送主题email.setMsg("1234");//设置发送内容email.send();//进行发送
            StringBuffer bodyBf = new StringBuffer();
            bodyBf.append("<!DOCTYPE html>");
            bodyBf.append("<HTML>");
            bodyBf.append("<HEAD>");
            bodyBf.append("<TITLE> 管理员你好</TITLE>");
            bodyBf.append("<meta http-equiv=Content-Type content=text/html; charset=utf-8>");
            bodyBf.append("</HEAD>");
            bodyBf.append("<BODY>");
            bodyBf.append("<h1 align=center style='color:rgb(5,112,255)'>服务端口监测系统 </h1>");
            bodyBf.append("<table border='1'  align=center>");
            bodyBf.append("<thead><tr>");
            bodyBf.append("<tr><th>名称</th><th>地址</th><th>端口</th></tr></thead><tbody>");
            for (int j=0;j<list.size();j++){
                String ip=list.get(j).getIp();
                String mc=list.get(j).getMc();
                String prot=list.get(j).getProt();
                bodyBf.append("<tr><th>"+mc+"</th><th>"+ip+"</th><th>"+prot+"</th></tr>");
            }
            bodyBf.append("</tbody></table>");
            bodyBf.append("</BODY>");
            bodyBf.append("</HTML>");
            //email.setMsg("您的验证码是:123456");
            email.setHtmlMsg(bodyBf.toString());
            email.send();
            System.out.println("发送成功");
        }else{

        }

    }
}
